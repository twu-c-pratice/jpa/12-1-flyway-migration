CREATE TABLE staff
(
    id         bigint      NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name varchar(16) NOT NULL,
    last_name  varchar(16) NOT NULL,
    office_id  bigint      NOT NULL,
    FOREIGN KEY (office_id) REFERENCES office (id)
);
