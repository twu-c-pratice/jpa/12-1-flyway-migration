CREATE TABLE office
(
    id      bigint      NOT NULL AUTO_INCREMENT PRIMARY KEY,
    country varchar(16) NOT NULL,
    city    varchar(16) NOT NULL
);
