CREATE TABLE contract
(
    id        bigint       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name      varchar(256) NOT NULL,
    branch_id bigint       NOT NULL,
    FOREIGN KEY (branch_id) REFERENCES branch (id)
);