CREATE TABLE branch
(
    id        bigint       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    client_id bigint       NOT NULL,
    name      varchar(128) NOT NULL,
    FOREIGN KEY (client_id) REFERENCES client (id)
)