CREATE TABLE contract_service_office
(
    id          bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
    contract_id bigint NOT NULL,
    office_id   bigint NOT NULL,
    FOREIGN KEY (contract_id) REFERENCES contract (id),
    FOREIGN KEY (office_id) REFERENCES office (id)
);
