CREATE TABLE client
(
    id           bigint       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    full_name    varchar(128) NOT NULL,
    abbreviation varchar(6)
);