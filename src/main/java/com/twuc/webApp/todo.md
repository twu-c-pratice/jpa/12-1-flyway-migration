# TODO

请按照如下的业务需求创建 migration 脚本。需要注意的是每一个需求都代表了一个特定时期的需求，必须得到支持。并在 migration 脚本中体现。

## 迭代 1

我们公司叫做 **SuperCool**。我们提供类似万事屋的服务。

我们的客户（*client*）是一个集团或者公司。例如 ThoughtWorks，或者 IBM。每一个客户都必须有一个全称（*full name*），并且有一个简称（*abbreviation*）。例如，ThoughtWorks 是全称，而 TWS 是简称。一般来说全称最长不超过 128 个字符而简称不会超过 6 个字符。

请书写关于客户的 migration 脚本。

## 迭代 2

我们和客户的业务开展是从一份服务合同（*contract*）开始的。需要注意的是我们的合同只针对一个客户在某一个具体的公司。例如，如果我们的客户是 ThoughtWorks，而一份具体的合同可能是和 ThoughtWorks Beijing 分公司签署的，而不是和整个 ThoughtWorks。我们会在合同的名称（*name*）上体现着一点。例如 *ThoughtWorks Beijing Service Contract*。

显然针对一个客户我们会签署非常多的服务合同。

请书写关于客户和服务合同的 migration 脚本。

## 迭代 3

我们现在准备为我们提供的服务（*service*）进行分类。例如现在就有三大类，分别是：**MOVIE、PARTY、BUSINESS_MEAL**。以后有没有新的类别就不好说了。但是应该是会继续添加的。为了增加运营效率我们决定每一份合同提供且只提供一种服务。这个服务一旦选定就不再更改了。

对于之前已经存在的合同来说，当时我们签的全都是 MOVIE。就全部都按照 MOVIE 处理好了。

请书写关于合同与服务的 migration 脚本。

## 迭代 4

一旦合同签订之后我们就需要组织资源来提供服务。我们先要说明一下我们的组织是怎么样的。我们在全世界有不计其数的办公室（*office*）。每一个办公室就按照所在的国家（*country*）和城市（*city*）来进行记录，不再额外命名。每一个办公室都有若干的员工（*staff*）。每一个员工必然属于且只属于一个办公室。每一个员工有 *firstname* 和 *lastname*。

请书写关于办公室和员工的 migration 脚本。

## 迭代 5

那么我们现在就需要为这些合同服务了。一个合同可能需要若干办公室的合作才能完成。假设对于 ThoughtWorks Beijing - MOVIE Contract 这个合同，我们指定了 China Beijing 和 China Xian 办公室来通力合作。这些办公室就叫做 *contract service offices*。显然，一个办公室可以充当多个 *contract* 的 *contract service office*；而一个 *contract service office* 必然由一个具体的 *office* 承担。

请书写关于 *contract service office* 及其相关结构的 migration 脚本。

## 迭代 6

虽然指定了 *contract service office* 但是我们仍然不能够放心。因为万一出了问题我应该找谁协调呢？因此我们需要为每一个 *contract service office* 指定负责人。这个负责人就叫做 *contract service office admin*。我们规定每一个 *contract service office admin* 只能够由相应 *office* 的员工承担。

请书写相关结构的 migration 脚本。